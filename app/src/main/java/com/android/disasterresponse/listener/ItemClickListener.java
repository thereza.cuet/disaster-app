package com.android.disasterresponse.listener;

import android.view.View;

public interface ItemClickListener {

    void onItemListener(View view, int position);
}
