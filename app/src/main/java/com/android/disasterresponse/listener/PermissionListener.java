package com.android.disasterresponse.listener;

public interface PermissionListener {
    void onPermissionGranted();
}
