package com.android.disasterresponse.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;

import com.android.disasterresponse.R;
import com.android.disasterresponse.data.preference.AppPreference;
import com.android.disasterresponse.data.preference.PrefKey;
import com.android.disasterresponse.model.ResponseSuccess;
import com.android.disasterresponse.model.UserResponse;
import com.android.disasterresponse.network.RetrofitClient;
import com.android.disasterresponse.utils.ActivityUtils;
import com.android.disasterresponse.utils.AppUtility;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private Context mContext;
    private Activity mActivity;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private ProgressDialog dialog;

    @BindView(R.id.parent)
    public ScrollView parent;

    @BindView(R.id.et_name)
    public EditText et_name;

    @BindView(R.id.et_address)
    public EditText et_address;
    @BindView(R.id.et_phone)
    public EditText et_phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVariable();
        initView();
        initFunctionality();
    }

    private void initVariable() {
        mContext = getApplicationContext();
        mActivity = LoginActivity.this;
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
    }

    private void initView() {
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }



    private void initFunctionality(){
        et_phone.setText(user.getPhoneNumber());
        checkUser();
    }


    @OnClick(R.id.cv_submit)
    public void onClickSubmit(View view){

        if (AppUtility.isNetworkAvailable(mContext)){
            if (!et_address.getText().toString().isEmpty() || !et_name.getText().toString().isEmpty()){
                submit();
            }
            else {
                AppUtility.showSnackBarMsg(mContext,parent,"You must be fillup name and address field.");
            }
        }
        else {
            AppUtility.showSnackBarMsg(mContext,parent,mContext.getString(R.string.no_internet));
        }
    }

    private void checkUser(){
        RetrofitClient.getClient().getUserByMobile(user.getPhoneNumber()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                if (response.body() != null) {
                    et_name.setText(response.body().getName());
                    et_address.setText(response.body().getAddress());
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {

            }
        });
    }


    public void submit(){
        dialog = ProgressDialog.show(mActivity,"","Please wait...",true);

        RetrofitClient.getClient().insertUser(et_name.getText().toString(),et_phone.getText().toString(),et_address.getText().toString()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                dialog.dismiss();
                AppPreference.getInstance(mContext).setString(PrefKey.KEY_NAME,response.body().getName());
                AppPreference.getInstance(mContext).setString(PrefKey.KEY_PHONE,response.body().getMobileNumber());
                AppPreference.getInstance(mContext).setString(PrefKey.KEY_USER_ID,response.body().getId());
                ActivityUtils.getInstance().invokeActivity(mActivity,MainActivity.class, true);
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext,t.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }



}
