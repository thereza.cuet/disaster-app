package com.android.disasterresponse.activity;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.disasterresponse.R;
import com.android.disasterresponse.adapter.NewsAdapter;
import com.android.disasterresponse.listener.ItemClickListener;
import com.android.disasterresponse.model.News;
import com.android.disasterresponse.network.RetrofitClient;
import com.android.disasterresponse.utils.AppUtility;
import com.android.disasterresponse.utils.MyDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.android.disasterresponse.data.AppConstants.NEWS_ID;

public class NewsActivity extends BaseActivity {

    private Activity mActivity;
    private Context mContext;

    private NewsAdapter  mAdapter;
    private ArrayList<News> dataList;

    @BindView(R.id.parent)
    public ConstraintLayout parent;

    @BindView(R.id.rv_news)
    public RecyclerView rv_news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initVariable();
        initView();
        initListener();

    }

    private void initVariable() {
        mActivity = NewsActivity.this;
        mContext = getApplicationContext();

        dataList = new ArrayList<>();
    }

    @SuppressLint("WrongConstant")
    private void initView() {

        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("সাম্প্রতিক খবর");

        initLoader();
        // use a linear layout manager since the cards are vertically scrollable
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_news.setLayoutManager(layoutManager);
        rv_news.addItemDecoration(new MyDividerItemDecoration(mContext, LinearLayoutManager.VERTICAL, 10));
        mAdapter = new NewsAdapter(mContext, dataList);
        rv_news.setAdapter(mAdapter);

        if (AppUtility.isNetworkAvailable(mContext))
            loadData();
        else
            AppUtility.showSnackBarMsg(mContext,parent,mContext.getString(R.string.no_internet));

    }

    private void initListener(){

        mAdapter.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemListener(View view, int position) {
                //Toast.makeText(mContext, "Clicked item "+ position, Toast.LENGTH_SHORT).show();
                Intent in = new Intent(mActivity, NewsDetailsActivity.class);
                in.putExtra(NEWS_ID,dataList.get(position).getId());
                startActivity(in);
            }
        });
    }
    private void loadData() {

        RetrofitClient.getClient().getAllNews().enqueue(new Callback<List<News>>() {
            @Override
            public void onResponse(Call<List<News>> call, Response<List<News>> response) {

                dataList.addAll(response.body());
                mAdapter.notifyDataSetChanged();
                hideLoader();
                if (dataList.isEmpty())
                    showEmptyView();
            }

            @Override
            public void onFailure(Call<List<News>> call, Throwable t) {
                showEmptyView();
            }
        });

    }
}
