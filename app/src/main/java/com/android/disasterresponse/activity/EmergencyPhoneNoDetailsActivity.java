package com.android.disasterresponse.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.android.disasterresponse.R;
import com.android.disasterresponse.adapter.EmergencyPhoneNoDetailsAdapter;
import com.android.disasterresponse.adapter.EmergencyPhoneNumberAdapter;
import com.android.disasterresponse.listener.ItemClickListener;
import com.android.disasterresponse.model.EmergencyPhoneCategory;
import com.android.disasterresponse.model.EmergencyPhoneNumber;
import com.android.disasterresponse.network.RetrofitClient;
import com.android.disasterresponse.utils.AppUtility;
import com.android.disasterresponse.utils.MyDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmergencyPhoneNoDetailsActivity extends BaseActivity {

    private Context mContext;
    private Activity mActivity;
    private ArrayList<EmergencyPhoneNumber> dataList;
    private EmergencyPhoneNoDetailsAdapter mAdapter;
    private String emergencyPhoneCatId, emergencyCatName;

    @BindView(R.id.parent)
    public ConstraintLayout parent;

    @BindView(R.id.rv_emergency_phone_no)
    public RecyclerView rv_emergency_phone_no;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initVariable();
        initView();
        initListener();
        enableBackButton();
    }

    private void initVariable(){

        mContext = getApplicationContext();
        mActivity = EmergencyPhoneNoDetailsActivity.this;

        dataList = new ArrayList<>();
        emergencyPhoneCatId = getIntent().getStringExtra("emergency_phone_cat_id");
        emergencyCatName = getIntent().getStringExtra("emergency_phone_title");
    }

    @SuppressLint("WrongConstant")
    private void initView() {
        setContentView(R.layout.activity_emergency_phone_no_details);
        initLoader();
        getSupportActionBar().setTitle(emergencyCatName);
        initEmptyView();
        initLoader();
        ButterKnife.bind(this);

        // use a linear layout manager since the cards are vertically scrollable
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_emergency_phone_no.setLayoutManager(layoutManager);
        rv_emergency_phone_no.addItemDecoration(new MyDividerItemDecoration(mContext, LinearLayoutManager.VERTICAL, 10));
        mAdapter = new EmergencyPhoneNoDetailsAdapter(mContext, dataList);
        rv_emergency_phone_no.setAdapter(mAdapter);

        if (AppUtility.isNetworkAvailable(mContext))
            loadData();
        else
            AppUtility.showSnackBarMsg(mContext,parent,mContext.getString(R.string.no_internet));

    }

    private void initListener() {
        mAdapter.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemListener(View view, int position) {
                AppUtility.phoneCall(mContext,"+88"+dataList.get(position).getMobileNumber());
            }
        });
    }

    private void loadData(){

        RetrofitClient.getClient().getEmergencyPhoneNo(emergencyPhoneCatId).enqueue(new Callback<List<EmergencyPhoneNumber>>() {
            @Override
            public void onResponse(Call<List<EmergencyPhoneNumber>> call, Response<List<EmergencyPhoneNumber>> response) {
                dataList.addAll(response.body());
                hideLoader();
                mAdapter.notifyDataSetChanged();
                if (dataList.isEmpty())
                    showEmptyView();
            }

            @Override
            public void onFailure(Call<List<EmergencyPhoneNumber>> call, Throwable t) {
                hideLoader();
                showEmptyView();
            }
        });
    }
}
