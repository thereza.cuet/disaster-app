package com.android.disasterresponse.activity;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AlertDialog;

import com.android.disasterresponse.R;
import com.android.disasterresponse.adapter.DistrictSpinnerAdapter;
import com.android.disasterresponse.adapter.DivisionSpinnerAdapter;
import com.android.disasterresponse.adapter.UnionSpinnerAdapter;
import com.android.disasterresponse.adapter.UpazilaSpinnerAdapter;
import com.android.disasterresponse.adapter.WardSpinnerAdapter;
import com.android.disasterresponse.data.AppConstants;
import com.android.disasterresponse.data.preference.AppPreference;
import com.android.disasterresponse.data.preference.PrefKey;
import com.android.disasterresponse.model.District;
import com.android.disasterresponse.model.Division;
import com.android.disasterresponse.model.ResponseSuccess;
import com.android.disasterresponse.model.Union;
import com.android.disasterresponse.model.Upazila;
import com.android.disasterresponse.model.Ward;
import com.android.disasterresponse.network.RetrofitClient;
import com.android.disasterresponse.upload_module.FileProgressReceiver;
import com.android.disasterresponse.upload_module.FileUploadService;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubmitDisasterInfoActivity extends BaseActivity {

    private Context mContext;
    private Activity mActivity;
    private Intent mIntent;
    private ProgressDialog dialog;
    private String disasterType;
    private String disasterTypeId;

    private ArrayList<Division> divisions;
    private ArrayList<District> districts;
    private ArrayList<Upazila> upazilas;
    private ArrayList<Union> unions;
    private ArrayList<Ward> wards;

    private DivisionSpinnerAdapter divAdapter;
    private DistrictSpinnerAdapter disAdapter;
    private UpazilaSpinnerAdapter upzAdapter;
    private UnionSpinnerAdapter unionAdapter;
    private WardSpinnerAdapter wardAdapter;

    private static final String IMAGE_DIRECTORY = "/disaster-app";

    private static final int CAMERA=1;
    private static final int GALARY=2;
    private static final int VIDEO=3;



    private Uri fileUri;
    String picturePath;

    Uri selectedImage = null;
    Bitmap photo;
    String selectedVideoPath;
    String filePath;

    @BindView(R.id.llParent)
    public LinearLayout llParent;

    @BindView(R.id.et_disaster_type)
    public EditText et_disaster_type;

    @BindView(R.id.et_details)
    public EditText et_details;

    @BindView(R.id.et_killed)
    public EditText et_killed;

    @BindView(R.id.et_injured)
    public EditText et_injured;

    @BindView(R.id.sp_div)
    public Spinner sp_div;
    @BindView(R.id.sp_dis)
    public Spinner sp_dis;
    @BindView(R.id.sp_upz)
    public Spinner sp_upz;
    @BindView(R.id.sp_union)
    public Spinner sp_union;
    @BindView(R.id.sp_word)
    public Spinner sp_word;

    @BindView(R.id.iv_camera)
    public ImageView iv_camera;

    @BindView(R.id.iv_video)
    public ImageView iv_video;

    @BindView(R.id.iv_preview)
    public ImageView iv_preview;

    @BindView(R.id.video_view)
    public VideoView video_view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVariable();
        initView();
        initListener();
    }

    private void initVariable() {
        mContext = getApplicationContext();
        mActivity = SubmitDisasterInfoActivity.this;
        mIntent = getIntent();

        divisions = new ArrayList<>();
        districts = new ArrayList<>();
        upazilas = new ArrayList<>();
        unions = new ArrayList<>();
        wards = new ArrayList<>();

        disasterType = mIntent.getStringExtra(AppConstants.DISASTER_TYPE_NAME);
        disasterTypeId = mIntent.getStringExtra(AppConstants.DISASTER_TYPE_ID);

    }

    private void initView() {
        setContentView(R.layout.activity_submit_disaster_info);
        ButterKnife.bind(this);
        et_disaster_type.setText(disasterType);

        getSupportActionBar().setTitle("দুর্যোগ তথ্য দিন");

        divAdapter = new DivisionSpinnerAdapter(mContext,divisions);
        sp_div.setAdapter(divAdapter);

        disAdapter = new DistrictSpinnerAdapter(mContext,districts);
        sp_dis.setAdapter(disAdapter);

        upzAdapter = new UpazilaSpinnerAdapter(mContext,upazilas);
        sp_upz.setAdapter(upzAdapter);

        unionAdapter = new UnionSpinnerAdapter(mContext,unions);
        sp_union.setAdapter(unionAdapter);

        wardAdapter = new WardSpinnerAdapter(mContext,wards);
        sp_word.setAdapter(wardAdapter);

        geDivision();

        //registerReceivers();
        //initializeBroadCastReceiver(iv_preview,video_view);
        //netConnectionAvailability(llParent);


    }

    private void initListener(){

        sp_div.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getDistrict(divisions.get(i).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sp_dis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getUpazila(districts.get(i).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sp_upz.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getUnion(upazilas.get(i).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sp_union.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getWard();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @OnClick(R.id.cv_submit)
    public void submitDisasterInfo(){

        String info = et_details.getText().toString();
        String killed = et_killed.getText().toString();
        String injured = et_injured.getText().toString();

        String divisionId = divisions.get(sp_div.getSelectedItemPosition()).getId();
        String disTrictId = districts.get(sp_dis.getSelectedItemPosition()).getId();
        String upzilaId = upazilas.get(sp_upz.getSelectedItemPosition()).getId();
        String unionId = unions.get(sp_union.getSelectedItemPosition()).getId();
        String wardId = wards.get(sp_word.getSelectedItemPosition()).getId();


//        if (selectedVideoPath != null){
//            filePath = selectedVideoPath;
//        }
//        if(selectedImage != null) {
//            filePath = ;
//        }

        File file = new File(filePath);
        RequestBody fileReqBdy = RequestBody.create(MediaType.parse("*/*"), file);
        MultipartBody.Part fileUpload = MultipartBody.Part.createFormData("file_url", file.getName(), fileReqBdy);

        Log.d("VideoPart",file.getName());

        RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), AppPreference.getInstance(mContext).getString(PrefKey.KEY_USER_ID));
        RequestBody typeId = RequestBody.create(MediaType.parse("text/plain"), disasterTypeId);
        RequestBody disasterName = RequestBody.create(MediaType.parse("text/plain"), disasterType);
        RequestBody disasterInfo = RequestBody.create(MediaType.parse("text/plain"), info);
        RequestBody totalKilled = RequestBody.create(MediaType.parse("text/plain"), killed);
        RequestBody totalInjured = RequestBody.create(MediaType.parse("text/plain"), injured);
        RequestBody divId = RequestBody.create(MediaType.parse("text/plain"), divisionId);
        RequestBody disId = RequestBody.create(MediaType.parse("text/plain"), disTrictId);
        RequestBody upzId = RequestBody.create(MediaType.parse("text/plain"), upzilaId);
        RequestBody uniId = RequestBody.create(MediaType.parse("text/plain"), unionId);
        RequestBody waId = RequestBody.create(MediaType.parse("text/plain"), wardId);

        dialog = ProgressDialog.show(mActivity,"","Please wait...",true);


//        Intent mIntent = new Intent(this, FileUploadService.class);
//        mIntent.putExtra("mFilePath", filePath);
//        mIntent.putExtra("divId", divisionId);
//        mIntent.putExtra("disId", disTrictId);
//        mIntent.putExtra("upzId", upzilaId);
//        mIntent.putExtra("unionId", unionId);
//        mIntent.putExtra("wardId", wardId);
//        mIntent.putExtra("userId", AppPreference.getInstance(mContext).getString(PrefKey.KEY_USER_ID));
//        mIntent.putExtra("disasterTypeId", disasterTypeId);
//        mIntent.putExtra("disasterName", disasterType);
//        FileUploadService.enqueueWork(this, mIntent);




        RetrofitClient.getClient().insertDisasterInfo(disasterName,disasterInfo,typeId,totalKilled,totalInjured,divId,disId,upzId,uniId,waId,userId,fileUpload).enqueue(new Callback<ResponseSuccess>() {
            @Override
            public void onResponse(Call<ResponseSuccess> call, Response<ResponseSuccess> response) {

                dialog.dismiss();
                //Toast.makeText(mContext,response.body().getMsg(),Toast.LENGTH_SHORT).show();
                Intent in = new Intent(mActivity,SubmitSuccessActivity.class);
                startActivity(in);
                iv_preview.setVisibility(View.GONE);
                finish();

            }

            @Override
            public void onFailure(Call<ResponseSuccess> call, Throwable t) {
                Toast.makeText(mContext,"দুঃখিত এই মূহুর্তে সংযোগ প্রদান করা সম্ভন হচ্ছে না। অনুগ্রহ করে আবার চেষ্টা করুন!",Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                Log.d("TestData",t.getLocalizedMessage());
            }
        });

    }


    @OnClick(R.id.iv_camera)
    public void clickPic() {
        selectImage();
    }
    private void selectImage() {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo"))
                {
                    Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, CAMERA);
                }
                else if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, GALARY);
                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    @OnClick(R.id.iv_video)
    public void chooseVideoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, VIDEO);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA) {

                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                iv_preview.setImageBitmap(thumbnail);
                iv_preview.setVisibility(View.VISIBLE);
                filePath = saveImage(thumbnail);

                Toast.makeText(mContext, "Image Saved!", Toast.LENGTH_SHORT).show();

                video_view.setVisibility(View.GONE);

            } else if (requestCode == GALARY) {
                selectedImage = data.getData();
                String[] filePaths = { MediaStore.Images.Media.DATA };
                Cursor c = getContentResolver().query(selectedImage,filePaths, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePaths[0]);
                picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                thumbnail=getResizedBitmap(thumbnail, 400);
                Log.w("path", picturePath+"");
                filePath = getRealPathFromURIPath(selectedImage, mActivity);
                iv_preview.setImageBitmap(thumbnail);
                iv_preview.setVisibility(View.VISIBLE);
                video_view.setVisibility(View.GONE);;
                iv_video.setEnabled(false);
                BitMapToString(thumbnail);
            }
            else if (requestCode == VIDEO) {
                Log.d("what","gale");
                if (data != null) {
                    Uri contentURI = data.getData();

                    filePath = getPath(contentURI,mActivity);
                    Log.d("path",selectedVideoPath);
                    video_view.setVideoURI(contentURI);
                    video_view.setVisibility(View.VISIBLE);
                    iv_preview.setVisibility(View.GONE);
                    iv_camera.setEnabled(false);
                    video_view.requestFocus();
                    video_view.start();
                }
            }
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    public String BitMapToString(Bitmap userImage1) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        userImage1.compress(Bitmap.CompressFormat.PNG, 60, baos);
        byte[] b = baos.toByteArray();
        picturePath = Base64.encodeToString(b, Base64.DEFAULT);
        return picturePath;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public String getPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Video.Media.DATA);
            return cursor.getString(idx);
        }
    }

    /*
     * This method is fetching the absolute path of the image file
     * if you want to upload other kind of files like .pdf, .docx
     * you need to make changes on this method only
     * Rest part will be the same
     * */
    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }


    @Override
    protected void onDestroy() {
        //unRegisterReceivers();
        super.onDestroy();
    }


    private void geDivision(){

        RetrofitClient.getClient().getDivision().enqueue(new Callback<List<Division>>() {
            @Override
            public void onResponse(Call<List<Division>> call, Response<List<Division>> response) {
                if (!divisions.isEmpty())
                    divisions.clear();

                divisions.addAll(response.body());
                divAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Division>> call, Throwable t) {

            }
        });
    }

    private void getDistrict(String divId){

        RetrofitClient.getClient().getDistrict(divId).enqueue(new Callback<List<District>>() {
            @Override
            public void onResponse(Call<List<District>> call, Response<List<District>> response) {

                if (!districts.isEmpty())
                    districts.clear();

                districts.addAll(response.body());
                disAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<District>> call, Throwable t) {

            }
        });
    }

    private void getUpazila(String disId){

        RetrofitClient.getClient().getUpazila(disId).enqueue(new Callback<List<Upazila>>() {
            @Override
            public void onResponse(Call<List<Upazila>> call, Response<List<Upazila>> response) {

                if (!upazilas.isEmpty())
                    upazilas.clear();

                upazilas.addAll(response.body());
                upzAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Upazila>> call, Throwable t) {

            }
        });
    }

    private void getUnion(String upzId){

        RetrofitClient.getClient().getUnion(upzId).enqueue(new Callback<List<Union>>() {
            @Override
            public void onResponse(Call<List<Union>> call, Response<List<Union>> response) {

                if (!unions.isEmpty())
                    unions.clear();

                unions.addAll(response.body());
                unionAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Union>> call, Throwable t) {

            }
        });
    }

    private void getWard(){

        RetrofitClient.getClient().getWard().enqueue(new Callback<List<Ward>>() {
            @Override
            public void onResponse(Call<List<Ward>> call, Response<List<Ward>> response) {
                if (!wards.isEmpty())
                    wards.clear();

                wards.addAll(response.body());
                wardAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Ward>> call, Throwable t) {

            }
        });
    }


}
