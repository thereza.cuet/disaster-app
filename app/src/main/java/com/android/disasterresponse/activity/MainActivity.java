package com.android.disasterresponse.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.android.disasterresponse.R;
import com.android.disasterresponse.data.AppConstants;
import com.android.disasterresponse.utils.ActivityUtils;
import com.android.disasterresponse.utils.AppUtility;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.view.View;

import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity{

    //Views
    private Activity mActivity;
    private Context mContext;

    @BindView(R.id.root_layout)
    DrawerLayout root_layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initVariable();
        initView();

    }

    private void initVariable(){
        mContext = getApplicationContext();
        mActivity = MainActivity.this;
    }


    private void initView() {
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initToolbar();
        initDrawer();
    }


    @OnClick(R.id.ll_disaster)
    public void clickDisasterInfo(View view){
        ActivityUtils.getInstance().invokeActivity(mActivity,DisasterTypeActivity.class,false);
    }

    @OnClick(R.id.ll_help)
    public void clickHelp(View view){
        ActivityUtils.getInstance().invokeActivity(mActivity,HelpActivity.class,false);
    }

    @OnClick(R.id.ll_emergency)
    public void clickEmergency(View view){
        ActivityUtils.getInstance().invokeActivity(mActivity,EmergencyContactActivity.class,false);
    }

    @OnClick(R.id.ll_hotline)
    public void onClickHotline(View view){

        AppUtility.phoneCall(mContext, AppConstants.ADMIN_NUMBER);
    }
    @OnClick(R.id.ll_news)
    public void onClickNews(View view){

        ActivityUtils.getInstance().invokeActivity(mActivity, NewsActivity.class, false);
    }
    @OnClick(R.id.ll_other)
    public void onClickOther(View view){

    }

    @Override
    protected void onResume() {
        super.onResume();
        AppUtility.askPermission(MainActivity.this);
        AppUtility.noInternetWarning(root_layout,mContext);
    }
}
