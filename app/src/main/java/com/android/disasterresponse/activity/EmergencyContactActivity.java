package com.android.disasterresponse.activity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.disasterresponse.R;
import com.android.disasterresponse.adapter.EmergencyPhoneNumberAdapter;
import com.android.disasterresponse.listener.ItemClickListener;
import com.android.disasterresponse.model.EmergencyPhoneCategory;
import com.android.disasterresponse.network.RetrofitClient;
import com.android.disasterresponse.utils.AppUtility;
import com.android.disasterresponse.utils.MyDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmergencyContactActivity extends BaseActivity {

    private Context mContext;
    private Activity mActivity;
    private ArrayList<EmergencyPhoneCategory> dataList;
    private EmergencyPhoneNumberAdapter mAdapter;

    @BindView(R.id.parent)
    public RelativeLayout parent;

    @BindView(R.id.rv_emergency)
    public RecyclerView rv_emergency;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVariable();
        initView();
        initListener();
        enableBackButton();

    }

    private void initVariable() {

        mActivity = EmergencyContactActivity.this;
        mContext = getApplicationContext();
        dataList = new ArrayList<>();

    }

    @SuppressLint("WrongConstant")
    private void initView() {

        setContentView(R.layout.activity_emergency_contact);
        getSupportActionBar().setTitle("জরুরী ফোন নম্বর");

        initLoader();
        ButterKnife.bind(this);
        // use a linear layout manager since the cards are vertically scrollable
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_emergency.setLayoutManager(layoutManager);
        rv_emergency.addItemDecoration(new MyDividerItemDecoration(mContext, LinearLayoutManager.VERTICAL, 10));
        mAdapter = new EmergencyPhoneNumberAdapter(mContext, dataList);
        rv_emergency.setAdapter(mAdapter);

        if (AppUtility.isNetworkAvailable(mContext))
            setupDummyData();
        else
            AppUtility.showSnackBarMsg(mContext,parent,mContext.getString(R.string.no_internet));


    }

    private void initListener(){

        mAdapter.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemListener(View view, int position) {

                Intent in = new Intent(mActivity, EmergencyPhoneNoDetailsActivity.class);
                in.putExtra("emergency_phone_cat_id",dataList.get(position).getId());
                in.putExtra("emergency_phone_title",dataList.get(position).getName());
                startActivity(in);

                //Toast.makeText(mContext, "Clicked item "+ position, Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void setupDummyData() {

        RetrofitClient.getClient().getEmergencyPhoneCategory().enqueue(new Callback<List<EmergencyPhoneCategory>>() {
            @Override
            public void onResponse(Call<List<EmergencyPhoneCategory>> call, Response<List<EmergencyPhoneCategory>> response) {
                dataList.addAll(response.body());
                hideLoader();
                mAdapter.notifyDataSetChanged();
                if (dataList.isEmpty())
                    showEmptyView();
            }

            @Override
            public void onFailure(Call<List<EmergencyPhoneCategory>> call, Throwable t) {

                showEmptyView();
            }
        });

    }
}
