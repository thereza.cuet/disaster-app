package com.android.disasterresponse.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.android.disasterresponse.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SubmitSuccessActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_success);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.cv_return)
    public void returnHome(){
        finish();
    }


}
