package com.android.disasterresponse.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.disasterresponse.R;
import com.android.disasterresponse.data.preference.AppPreference;
import com.android.disasterresponse.data.preference.PrefKey;
import com.android.disasterresponse.model.ResponseSuccess;
import com.android.disasterresponse.network.RetrofitClient;
import com.android.disasterresponse.utils.AppUtility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HelpActivity extends BaseActivity {

    private Activity mActivity;
    private Context mContext;
    private ProgressDialog dialog;

    @BindView(R.id.et_details)
    EditText et_details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initVariable();
        initView();
        initListener();
    }

    public void initVariable(){
        mActivity = HelpActivity.this;
        mContext = getApplicationContext();
    }

    private void initView() {
        setContentView(R.layout.activity_help);
        getSupportActionBar().setTitle("সাহায্যের আবেদন");
        ButterKnife.bind(this);
    }

    private void initListener() {

    }

    @OnClick(R.id.cv_submit_help_req)
    public void submitHelpReq(View view){

        if(et_details.getText().toString().equals("")){
            et_details.setError("Please fillup details field!");
        }
        else {

            String helpDetails = et_details.getText().toString();

            if (!AppUtility.isNetworkAvailable(mContext)){

                AppUtility.appClosePrompt(mActivity,mContext,helpDetails);

            }
            else {
                dialog = ProgressDialog.show(mActivity, "", "Please wait...", true);
                RetrofitClient.getClient().insertHelpRequest(AppPreference.getInstance(mContext).getString(PrefKey.KEY_USER_ID),helpDetails).enqueue(new Callback<ResponseSuccess>() {
                    @Override
                    public void onResponse(Call<ResponseSuccess> call, Response<ResponseSuccess> response) {
                        dialog.dismiss();
                        //Toast.makeText(mContext,response.body().getMsg(),Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(mActivity,SubmitSuccessActivity.class);
                        startActivity(in);
                        finish();
                    }

                    @Override
                    public void onFailure(Call<ResponseSuccess> call, Throwable t) {
                        Toast.makeText(mContext,"দুঃখিত এই মূহুর্তে সংযোগ প্রদান করা সম্ভন হচ্ছে না। অনুগ্রহ করে আবার চেষ্টা করুন!",Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        Log.d("TestData",t.getLocalizedMessage());
                    }
                });
            }
        }

    }


}
