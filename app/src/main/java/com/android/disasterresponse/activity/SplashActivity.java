package com.android.disasterresponse.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.disasterresponse.R;
import com.android.disasterresponse.data.AppConstants;
import com.android.disasterresponse.data.preference.AppPreference;
import com.android.disasterresponse.data.preference.PrefKey;
import com.android.disasterresponse.utils.AppUtility;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.BuildConfig;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    private static final int RC_SIGN_IN = 101;
    private Context mContext;
    private Activity mActivity;
    private FirebaseAuth mAuth;
    @BindView(R.id.llParent)
    public ConstraintLayout llParent;

    @BindView(R.id.pb_loader)
    public ProgressBar pb_loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        initVariable();
        initView();
        initFunctionality();
    }

    private void initVariable(){

        mContext = getApplicationContext();
        mActivity = SplashActivity.this;

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
    }
    private void initView(){
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
    }

    private void initFunctionality() {

            //final FirebaseUser user = mAuth.getCurrentUser();
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {

                        sleep(1000);
                        if (/*user == null ||*/ AppPreference.getInstance(mContext).getString(PrefKey.KEY_USER_ID) == null) {
                            doPhoneLogin();

                        } else {
                            Intent intent = new Intent(mActivity,MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
//                        Intent intent = new Intent(mActivity,MainActivity.class);
//                        startActivity(intent);
//                        finish();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    super.run();
                }
            };
            thread.start();
        //}
        /*else {
            AppUtility.noInternetWarning(llParent, mContext);
            pb_loader.setVisibility(View.INVISIBLE);
        }*/

    }



    private void doPhoneLogin() {
        Intent intent = AuthUI.getInstance().createSignInIntentBuilder()
                .setIsSmartLockEnabled(!BuildConfig.DEBUG)
                .setAvailableProviders(Collections.singletonList(
                        new AuthUI.IdpConfig.PhoneBuilder().build()))
                .setLogo(R.mipmap.ic_launcher)
                .build();

        startActivityForResult(intent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            IdpResponse idpResponse = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                // Successfully signed in
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                //showAlertDialog(user);
                Intent intent = new Intent(this,LoginActivity.class);
                startActivity(intent);
                finish();

            } else {
                /**
                 *   Sign in failed. If response is null the user canceled the
                 *   sign-in flow using the back button. Otherwise check
                 *   response.getError().getErrorCode() and handle the error.
                 */
                Toast.makeText(getBaseContext(), "Phone Auth Failed", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppUtility.noInternetWarning(llParent, mContext);
    }


}
