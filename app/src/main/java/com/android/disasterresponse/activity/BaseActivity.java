package com.android.disasterresponse.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.disasterresponse.R;
import com.android.disasterresponse.data.AppConstants;
import com.android.disasterresponse.data.preference.AppPreference;
import com.android.disasterresponse.data.preference.PrefKey;
import com.android.disasterresponse.listener.PermissionListener;
import com.android.disasterresponse.utils.ActivityUtils;
import com.android.disasterresponse.utils.AppUtility;
import com.android.disasterresponse.utils.NetworkUtil;
import com.android.disasterresponse.utils.PermissionUtils;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, PermissionListener {

    private Context mContext;
    private Activity mActivity;

    private BroadcastReceiver myBroadCastReceiver, mInternetConnectivityChangeReceiver;

    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private LinearLayout loadingView, noDataView;
    private RelativeLayout loaderParent;
    public TextView tv_phone_no,tv_user_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVariable();
    }

    private void initVariable() {
        mContext = getApplicationContext();
        mActivity = BaseActivity.this;

    }

    public void initToolbar() {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    public void enableBackButton() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    public Toolbar getToolbar() {
        if (mToolbar == null) {
            mToolbar = findViewById(R.id.toolbar);
            setSupportActionBar(mToolbar);
        }
        return mToolbar;
    }

    public void setToolBarTittle(String toolBarTitle) {
        TextView toolTitle = findViewById(R.id.toolbar);
        toolTitle.setText(toolBarTitle);

    }

    public void initLoader() {
        loadingView = findViewById(R.id.loadingView);
        noDataView = findViewById(R.id.noDataView);
        loaderParent = findViewById(R.id.loaderParent);
    }

    public void hideLoader() {
        if (loadingView != null) {
            loadingView.setVisibility(View.GONE);
        }
        if (noDataView != null) {
            noDataView.setVisibility(View.GONE);
        }
        if (loaderParent != null) {
            loaderParent.setVisibility(View.GONE);
        }
    }

    public void initDrawer() {

        mDrawerLayout = findViewById(R.id.root_layout);
        mNavigationView = findViewById(R.id.nav_view);
        View headerView = mNavigationView.getHeaderView(0);


        tv_phone_no = headerView.findViewById(R.id.tv_phone_no);
        tv_user_name = headerView.findViewById(R.id.tv_user_name);


        mNavigationView.setNavigationItemSelectedListener(this);
        mNavigationView.setItemIconTintList(null);

        tv_user_name.setText(AppPreference.getInstance(mContext).getString(PrefKey.KEY_NAME));
        tv_phone_no.setText(AppPreference.getInstance(mContext).getString(PrefKey.KEY_PHONE));



        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        toggle.syncState();
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });
    }

    public void initEmptyView() {
        noDataView = (LinearLayout) findViewById(R.id.noDataView);
    }


    public void showEmptyView() {
        if (noDataView != null) {
            noDataView.setVisibility(View.VISIBLE);
        }
    }

    public void hideEmptyView() {
        if (noDataView != null) {
            noDataView.setVisibility(View.GONE);
        }
        if (loaderParent != null) {
            loaderParent.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            mDrawerLayout.closeDrawer(GravityCompat.START);

        } else if (id == R.id.nav_logout) {

            FirebaseAuth.getInstance().signOut();
            finish();

            if (mDrawerLayout.isDrawerOpen(mNavigationView)){
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }

        } else if (id == R.id.nav_share) {
            AppUtility.shareApp(mActivity);
            mDrawerLayout.closeDrawer(GravityCompat.START);

        } else if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (PermissionUtils.isPermissionResultGranted(grantResults)) {
            if (requestCode == PermissionUtils.PERMISSION_REQUEST_CALLBACK_CONSTANT) {
                AppUtility.askPermission(mActivity);
            }
        } else {
            AppUtility.showToast(mActivity, getResources().getString(R.string.permission_not_granted));
        }

    }

    public void registerReceivers() {
        LocalBroadcastManager.getInstance(this).registerReceiver(myBroadCastReceiver, new IntentFilter("MyBroadCastReceiver"));
        registerReceiver(mInternetConnectivityChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    public void unRegisterReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myBroadCastReceiver);
        unregisterReceiver(mInternetConnectivityChangeReceiver);
    }


    public void initializeBroadCastReceiver(ImageView view, VideoView videoView) {
        myBroadCastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String status = intent.getStringExtra("STATUS");
                switch (status) {
                    case "Success":
                        view.setVisibility(View.GONE);
                        videoView.setVisibility(View.GONE);
                        break;
                }

            }
        };
    }

    //Net connection check and player play/pause
    public void netConnectionAvailability(View view) {
        mInternetConnectivityChangeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                int status = NetworkUtil.getConnectivityStatusInt(context);
                if (status == AppConstants.VALUE_ZERO) {
                    AppUtility.showSnackBarMsg(mContext,view,mContext.getString(R.string.no_internet));
                } else {

                    if (isInitialStickyBroadcast()) {
                        // Do Nothing;
                    } else {
                    }

                }
            }
        };
    }




    @Override
    public void onBackPressed() {
        if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onPermissionGranted() {

    }
}