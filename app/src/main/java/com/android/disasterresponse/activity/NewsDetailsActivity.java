package com.android.disasterresponse.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.disasterresponse.R;
import com.android.disasterresponse.model.News;
import com.android.disasterresponse.network.RetrofitClient;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.android.disasterresponse.data.AppConstants.NEWS_ID;

public class NewsDetailsActivity extends BaseActivity {

    // init variables
    private Context mContext;
    private Activity mActivity;
    private ArrayList<News> dataList;
    private TextView tvPostTitle, tvPostAuthor, tvPostDate, tvPostDetails;
    private ImageView imgPost;
    private String newsId;

    @BindView(R.id.lyt_post_details)
    public RelativeLayout lytPostDetailsView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVariable();
        initView();
    }

    private void initVariable() {
        mActivity = NewsDetailsActivity.this;
        mContext = mActivity.getApplicationContext();

        newsId = getIntent().getStringExtra(NEWS_ID);

        dataList = new ArrayList<>();
    }
    private void initView(){

        setContentView(R.layout.activity_news_details);
        imgPost = (ImageView) findViewById(R.id.post_img);
        tvPostTitle = (TextView) findViewById(R.id.title_text);
        tvPostAuthor = (TextView) findViewById(R.id.post_author);
        tvPostDate = (TextView) findViewById(R.id.date_text);
        tvPostDetails = findViewById(R.id.tv_post_details);

        ButterKnife.bind(this);

        initLoader();
        initToolbar();
        enableBackButton();

        loadPostDetails();
    }


    @OnClick(R.id.imgBtnShare)
    public void onClickShare(){
        if (!dataList.isEmpty()) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, dataList.get(0).getTitle());
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
        }
    }

    public void loadPostDetails() {

        RetrofitClient.getClient().getSingleNews(newsId).enqueue(new Callback<List<News>>() {
            @Override
            public void onResponse(Call<List<News>> call, Response<List<News>> response) {

                dataList.addAll(response.body());

                Log.d("test",new Gson().toJson(response.body()));

                hideLoader();
                // visible parent view
                lytPostDetailsView.setVisibility(View.VISIBLE);

                tvPostTitle.setText(Html.fromHtml(dataList.get(0).getTitle()));

                if (dataList.get(0).getImgUrl() != null) {
                    Glide.with(getApplicationContext())
                            .load(dataList.get(0).getImgUrl())
                            .into(imgPost);
                }

                String author = null;
                if (author == null) {
                    author = "Admin";
                }
                tvPostAuthor.setText(Html.fromHtml(author));

                String oldDate = dataList.get(0).getPublishDate();
                tvPostDate.setText(Html.fromHtml(oldDate));

                tvPostDetails.setText(dataList.get(0).getDescription());
            }

            @Override
            public void onFailure(Call<List<News>> call, Throwable t) {

                Log.d("test",t.getLocalizedMessage());
                showEmptyView();
                hideLoader();
            }
        });


    }
}
