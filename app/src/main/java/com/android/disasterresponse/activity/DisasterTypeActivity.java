package com.android.disasterresponse.activity;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import com.android.disasterresponse.R;
import com.android.disasterresponse.adapter.DisasterTypeAdapter;
import com.android.disasterresponse.data.AppConstants;
import com.android.disasterresponse.listener.ItemClickListener;
import com.android.disasterresponse.model.DisasterType;
import com.android.disasterresponse.network.RetrofitClient;
import com.android.disasterresponse.utils.ActivityUtils;
import com.android.disasterresponse.utils.AppUtility;
import com.android.disasterresponse.utils.GridSpacingItemDecoration;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DisasterTypeActivity extends BaseActivity {

    private Activity mActivity;
    private Context mContext;

    private DisasterTypeAdapter mAdapter;
    private ArrayList<DisasterType> dataList;

    @BindView(R.id.rv_disaster_type)
    public RecyclerView rv_disaster_type;
    @BindView(R.id.parent)
    public ConstraintLayout parent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initVariable();
        initView();
        initListener();
        enableBackButton();
    }

    private void initVariable() {
        mActivity = DisasterTypeActivity.this;
        mContext = getApplicationContext();

        dataList = new ArrayList<>();
    }

    @SuppressLint("WrongConstant")
    private void initView() {

        setContentView(R.layout.activity_disaster_type);
        ButterKnife.bind(this);

        initLoader();

        //Channel List Initialization
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
        rv_disaster_type.setLayoutManager(mLayoutManager);
        rv_disaster_type.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(5), true));
        rv_disaster_type.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new DisasterTypeAdapter(mContext,dataList);
        rv_disaster_type.setAdapter(mAdapter);

        if (AppUtility.isNetworkAvailable(mContext))
            loadData();
        else
            AppUtility.showSnackBarMsg(mContext,parent,mContext.getString(R.string.no_internet));

    }

    private void initListener(){

        mAdapter.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemListener(View view, int position) {

                ActivityUtils.getInstance().invokeActivityWithPutExtra(mActivity,SubmitDisasterInfoActivity.class,dataList.get(position).getId(), dataList.get(position).getName());
            }
        });
    }
    private void loadData() {
        RetrofitClient.getClient().getDisasterType().enqueue(new Callback<List<DisasterType>>() {
            @Override
            public void onResponse(Call<List<DisasterType>> call, Response<List<DisasterType>> response) {

                dataList.addAll(response.body());
                mAdapter.notifyDataSetChanged();

                Log.d("Test", new Gson().toJson(response.body()));
                hideLoader();
                if (dataList.isEmpty())
                    showEmptyView();
            }

            @Override
            public void onFailure(Call<List<DisasterType>> call, Throwable t) {
                Log.d("Test",t.getLocalizedMessage());
                showEmptyView();
                hideLoader();
            }
        });

    }
    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


}
