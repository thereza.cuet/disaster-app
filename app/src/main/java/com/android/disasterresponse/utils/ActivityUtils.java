package com.android.disasterresponse.utils;

import android.app.Activity;
import android.content.Intent;

import com.android.disasterresponse.data.AppConstants;

public class ActivityUtils {
    private static ActivityUtils sActivityUtils = null;

    public static ActivityUtils getInstance() {
        if (sActivityUtils == null) {
            sActivityUtils = new ActivityUtils();
        }
        return sActivityUtils;
    }

    public void invokeActivity(Activity activity, Class<?> tClass, boolean shouldFinish) {
        Intent intent = new Intent(activity, tClass);
        activity.startActivity(intent);
        if (shouldFinish) {
            activity.finish();
        }
    }

    public void invokeActivityWithPutExtra(Activity activity, Class<?> tClass,String id, String name){

        Intent intent = new Intent(activity, tClass);
        intent.putExtra(AppConstants.DISASTER_TYPE_ID,id);
        intent.putExtra(AppConstants.DISASTER_TYPE_NAME,name);
        activity.startActivity(intent);
    }
}
