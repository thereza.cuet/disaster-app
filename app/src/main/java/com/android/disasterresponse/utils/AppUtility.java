package com.android.disasterresponse.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.disasterresponse.R;
import com.android.disasterresponse.data.AppConstants;
import com.android.disasterresponse.listener.PermissionListener;
import com.google.android.material.snackbar.Snackbar;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;


public class AppUtility {

    private static AppUtility appUtilities = null;

    // create single instance
    public static AppUtility getInstance() {
        if (appUtilities == null) {
            appUtilities = new AppUtility();
        }
        return appUtilities;
    }

    public static void askPermission(Activity activity) {

        if (PermissionUtils.isPermissionGranted(activity, PermissionUtils.PERMISSIONS_REQUIRED, PermissionUtils.PERMISSION_REQUEST_CALLBACK_CONSTANT)) {
            PermissionListener permissionListener = (PermissionListener) activity;
            permissionListener.onPermissionGranted();

        }

    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }



    @SuppressLint("MissingPermission")
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
    public static void noInternetWarning(View view, final Context context) {
        if (!isNetworkAvailable(context)) {
            Snackbar snackbar = Snackbar.make(view, context.getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction(context.getString(R.string.connect), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
            snackbar.show();
        }
    }


    public static void sendBroadCastMessages(Context mContext, String statusMessages) {
        Intent broadCastIntent = new Intent("MyBroadCastReceiver");
        broadCastIntent.putExtra("STATUS", statusMessages);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(broadCastIntent);
    }


    // common snack bar bellow different view and necessary message
    public static void showSnackBarMsg(Context context, View view, String msg) {
        Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    public static void shareApp(Activity activity) {
        try {
            final String appPackageName = activity.getPackageName();
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, activity.getResources().getString(R.string.share) + "https://play.google.com/store/apps/details?id=" + appPackageName);
            sendIntent.setType("text/plain");
            activity.startActivity(sendIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void rateThisApp(Activity activity) {
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + activity.getPackageName())));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void phoneCall(Context mContext, String number) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.addFlags(FLAG_ACTIVITY_NEW_TASK);
        callIntent.setData(Uri.parse("tel:"+number));
        mContext.startActivity(callIntent);
    }

    public static void sendSMS(Context mContext, String phoneNo, String msg) {
        /*try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(mContext, "Message Sent",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(mContext,ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }*/

        Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
        smsIntent.setFlags(FLAG_ACTIVITY_NEW_TASK);
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address",phoneNo);
        smsIntent.putExtra("sms_body",msg);
        mContext.startActivity(smsIntent);
    }


    public static void appClosePrompt(final Activity activity, Context mContext, String msg) {
        DialogUtils.showDialogPrompt(activity, null, activity.getString(R.string.send_msg_string), activity.getString(R.string.yes), activity.getString(R.string.no), true, new DialogUtils.DialogActionListener() {
            @Override
            public void onPositiveClick() {
                sendSMS(mContext,AppConstants.ADMIN_NUMBER,msg);
            }
        });
    }


}
