package com.android.disasterresponse.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.disasterresponse.R;

import static android.os.Build.VERSION_CODES.M;

/**
 * Created by theReza on 6/1/16.
 */
public class PermissionUtils {

    public static final int PERMISSION_REQUEST_CALLBACK_CONSTANT = 112;

    public static String[] PERMISSIONS_REQUIRED = new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE};

    public static boolean isPermissionGranted(Activity activity, String[] permissions, int requestCode) {
        boolean requirePermission = false;
        if(permissions != null && permissions.length > 0) {
            for (String permission : permissions) {
                if ((ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED)) {
                    requirePermission = true;
                    break;
                }
            }
        }

        if (requirePermission) {
            ActivityCompat.requestPermissions(activity, permissions, requestCode);
            return false;
        } else {
            return true;
        }
    }

    public static boolean isPermissionResultGranted(int[] grantResults) {
        boolean allGranted = true;
        if(grantResults != null && grantResults.length > 0) {
            for (int i : grantResults) {
                if(i != PackageManager.PERMISSION_GRANTED) {
                    allGranted = false;
                    break;
                }
            }
        }
        return allGranted;
    }

    public static boolean hasPermission(Context context, String permission) {
        if (Build.VERSION.SDK_INT < M) {
            return true;
        }
        if (context == null || TextUtils.isEmpty(permission)) {
            return false;
        }
        return ContextCompat.checkSelfPermission(context, permission)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestPermission(Activity activity, String permission, int resultCode) {
        if (Build.VERSION.SDK_INT < M) {
            return;
        }
        if (activity == null || TextUtils.isEmpty(permission)) {
            return;
        }
        ActivityCompat.requestPermissions(activity, new String[]{permission}, resultCode);
    }

    public static void showDialog(Activity activity, String title, final DialogInterface.OnClickListener onClickListener){

        if(activity != null) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity, R.style.Theme_AppCompat_Dialog_Alert);
            if (title != null) {
                alertDialogBuilder.setTitle(title);
            }

            if (onClickListener != null) {
                alertDialogBuilder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        //onClickListener.onPositiveClick();
                    }
                });
            }

            if (onClickListener != null) {
                alertDialogBuilder.setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
            }

            alertDialogBuilder.create().show();
        }
    }
}

