package com.android.disasterresponse.data.preference;

/**
 * Created by theReza on 5/16/16.
 */
public class PrefKey {

    public static final String APP_PREF_NAME = "disaster_app";

    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_NAME = "name";

}
