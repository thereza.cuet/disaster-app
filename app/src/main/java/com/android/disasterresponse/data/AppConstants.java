package com.android.disasterresponse.data;

public class AppConstants {

    public static final String DISASTER_TYPE_ID = "disaster_type_id";
    public static final String DISASTER_TYPE_NAME = "disaster_type_name";
    public static final String USER_PHONE_NO = "phone_no";

    public static final String NEWS_ID = "news_id";
    public static final int VALUE_ZERO = 0;
    public static final String ADMIN_NUMBER = "01711832411";
}
