package com.android.disasterresponse.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmergencyPhoneNumber {
    @SerializedName("helpline_number_id")
    @Expose
    private String helplineNumberId;
    @SerializedName("phone_number")
    @Expose
    private Object phoneNumber;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("helpline_category_type_id")
    @Expose
    private String helplineCategoryTypeId;
    @SerializedName("helpline_category_name")
    @Expose
    private String helplineCategoryName;

    public String getHelplineNumberId() {
        return helplineNumberId;
    }

    public void setHelplineNumberId(String helplineNumberId) {
        this.helplineNumberId = helplineNumberId;
    }

    public Object getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Object phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHelplineCategoryTypeId() {
        return helplineCategoryTypeId;
    }

    public void setHelplineCategoryTypeId(String helplineCategoryTypeId) {
        this.helplineCategoryTypeId = helplineCategoryTypeId;
    }

    public String getHelplineCategoryName() {
        return helplineCategoryName;
    }

    public void setHelplineCategoryName(String helplineCategoryName) {
        this.helplineCategoryName = helplineCategoryName;
    }
}
