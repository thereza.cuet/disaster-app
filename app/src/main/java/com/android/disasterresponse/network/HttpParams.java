package com.android.disasterresponse.network;

public class HttpParams {

    public static final String BASE_URL = "https://starlightcapitalplus.com/uno/DisasterApi/";
    public static final String DISASTER_TYPE_ENDPOINT = "getAllDisasterType";
    public static final String INSERT_USER_ENDPOINT = "userInsert";
    public static final String GET_USER_ENDPOINT = "getUserByMobile";
    public static final String ALL_NEWS_ENDPOINT = "getAllNews";
    public static final String SINGLE_NEWS_ENDPOINT = "getSingleNews";
    public static final String INSERT_DISASTER_INFO_ENDPOINT = "insertDisasterInfo";
    public static final String INSERT_HELP_REQUEST_ENDPOINT = "insertHelpRequest";
    public static final String EMERGENCY_PHONE_ENDPOINT = "getEmergencyPhone/{id}";
    public static final String EMERGENCY_PHONE_CATEGORY_ENDPOINT = "getEmergencyPhoneCategory";

    public static final String DIVISION_ENDPOINT = "getDivision";
    public static final String DISTRICT_ENDPOINT = "getDistrict";
    public static final String UPZ_ENDPOINT = "getUpazila";
    public static final String UNION_ENDPOINT = "getUnion";
    public static final String WARD_ENDPOINT = "getWard";

    public static final String NAME = "name";
    public static final String MOBILE_NO = "mobile_number";
    public static final String ADDRESS = "address";
    public static final String DESCRIPTION = "description";
    public static final String CREATED_BY = "created_by";
    public static final String TITLE = "title";
    public static final String DISASTER_TYPE_ID = "disaster_type_id";
    public static final String IMG_URL = "img_url";
    public static final String VIDEO_URL = "video_url";

    public static final String DIV_ID = "division_id";
    public static final String DIS_ID = "district_id";
    public static final String UNION_ID = "union_id";
    public static final String WARD_ID = "word_id";
    public static final String UPZ_ID = "upazila_id";
    public static final String KILLED = "total_killed";
    public static final String INJURED = "total_injured";
    public static final String NEWS_ID = "news_id";
}

