package com.android.disasterresponse.network;

import com.android.disasterresponse.model.DisasterType;
import com.android.disasterresponse.model.District;
import com.android.disasterresponse.model.Division;
import com.android.disasterresponse.model.EmergencyPhoneCategory;
import com.android.disasterresponse.model.EmergencyPhoneNumber;
import com.android.disasterresponse.model.News;
import com.android.disasterresponse.model.ResponseSuccess;
import com.android.disasterresponse.model.Union;
import com.android.disasterresponse.model.Upazila;
import com.android.disasterresponse.model.UserResponse;
import com.android.disasterresponse.model.Ward;

import java.util.List;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiInterface {

    @FormUrlEncoded
    @POST(HttpParams.GET_USER_ENDPOINT)
    Call<UserResponse> getUserByMobile(@Field(HttpParams.MOBILE_NO) String mobile);

    @FormUrlEncoded
    @POST(HttpParams.INSERT_USER_ENDPOINT)
    Call<UserResponse> insertUser(
            @Field(HttpParams.NAME) String name,
            @Field(HttpParams.MOBILE_NO) String mobileNo,
            @Field(HttpParams.ADDRESS) String address
    );

    @GET(HttpParams.DISASTER_TYPE_ENDPOINT)
    Call<List<DisasterType>> getDisasterType();

    /*@Multipart
    @POST(HttpParams.INSERT_DISASTER_INFO_ENDPOINT)
    Single<ResponseBody> insertDisasterInfo(
            @Part(HttpParams.TITLE) RequestBody title,
            @Part(HttpParams.DESCRIPTION) RequestBody description,
            @Part(HttpParams.DISASTER_TYPE_ID) RequestBody disasterTypeId,
            @Part(HttpParams.DIV_ID) RequestBody divId,
            @Part(HttpParams.DIS_ID) RequestBody disId,
            @Part(HttpParams.UPZ_ID) RequestBody upzId,
            @Part(HttpParams.UNION_ID) RequestBody unionID,
            @Part(HttpParams.WARD_ID) RequestBody wardId,
            @Part(HttpParams.CREATED_BY) RequestBody userId,
            @Part MultipartBody.Part image_url
            //@Part MultipartBody.Part video
    );*/

    @Multipart
    @POST(HttpParams.INSERT_DISASTER_INFO_ENDPOINT)
    Call<ResponseSuccess> insertDisasterInfo(
            @Part(HttpParams.TITLE) RequestBody title,
            @Part(HttpParams.DESCRIPTION) RequestBody description,
            @Part(HttpParams.DISASTER_TYPE_ID) RequestBody disasterTypeId,
            @Part(HttpParams.KILLED) RequestBody killed,
            @Part(HttpParams.INJURED) RequestBody injured,
            @Part(HttpParams.DIV_ID) RequestBody divId,
            @Part(HttpParams.DIS_ID) RequestBody disId,
            @Part(HttpParams.UPZ_ID) RequestBody upzId,
            @Part(HttpParams.UNION_ID) RequestBody unionID,
            @Part(HttpParams.WARD_ID) RequestBody wardId,
            @Part(HttpParams.CREATED_BY) RequestBody userId,
            @Part MultipartBody.Part image_url
    );

    @FormUrlEncoded
    @POST(HttpParams.INSERT_HELP_REQUEST_ENDPOINT)
    Call<ResponseSuccess> insertHelpRequest(
            @Field(HttpParams.CREATED_BY) String userId,
            @Field(HttpParams.DESCRIPTION) String description
    );


    @GET(HttpParams.ALL_NEWS_ENDPOINT)
    Call<List<News>> getAllNews();

    @FormUrlEncoded
    @POST(HttpParams.SINGLE_NEWS_ENDPOINT)
    Call<List<News>> getSingleNews(@Field(HttpParams.NEWS_ID) String id);

    @GET(HttpParams.EMERGENCY_PHONE_CATEGORY_ENDPOINT)
    Call<List<EmergencyPhoneCategory>> getEmergencyPhoneCategory();

    @GET(HttpParams.EMERGENCY_PHONE_ENDPOINT)
    Call<List<EmergencyPhoneNumber>> getEmergencyPhoneNo(@Path("id") String id);

    @GET(HttpParams.DIVISION_ENDPOINT)
    Call<List<Division>> getDivision();

    @GET(HttpParams.WARD_ENDPOINT)
    Call<List<Ward>> getWard();

    @FormUrlEncoded
    @POST(HttpParams.DISTRICT_ENDPOINT)
    Call<List<District>> getDistrict(@Field(HttpParams.DIV_ID) String divId);

    @FormUrlEncoded
    @POST(HttpParams.UPZ_ENDPOINT)
    Call<List<Upazila>> getUpazila(@Field(HttpParams.DIS_ID) String disID);

    @FormUrlEncoded
    @POST(HttpParams.UNION_ENDPOINT)
    Call<List<Union>> getUnion(@Field(HttpParams.UPZ_ID) String upzId);


//    @GET(HttpParams.SHEET_API_END_POINT)
//    Call<ChannelList> getChannelList(@Query("id") String sheetId, @Query("sheet") String sheetName);
}
