package com.android.disasterresponse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.disasterresponse.R;
import com.android.disasterresponse.listener.ItemClickListener;
import com.android.disasterresponse.model.News;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<News> dataList;
    // Listener
    public ItemClickListener mListener;


    public NewsAdapter(Context mContext, ArrayList<News> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public NewsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull NewsAdapter.MyViewHolder holder, int position) {

        News news = dataList.get(position);

        holder.tv_news_title.setText(news.getTitle());
        Glide
                .with(mContext)
                .load(news.getImgUrl())
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.iv_news);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.iv_news)
        public ImageView iv_news;
        @BindView(R.id.tv_news_title)
        public TextView tv_news_title;

        public MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemListener(itemView,getAdapterPosition());
                }
            });
        }
    }

    public void setItemClickListener(ItemClickListener mListener) {
        if (mListener != null) {
            this.mListener = mListener;
        }
    }

}
