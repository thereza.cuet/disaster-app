package com.android.disasterresponse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.disasterresponse.R;
import com.android.disasterresponse.listener.ItemClickListener;
import com.android.disasterresponse.model.EmergencyPhoneCategory;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmergencyPhoneNumberAdapter extends RecyclerView.Adapter<EmergencyPhoneNumberAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<EmergencyPhoneCategory> dataList;
    // Listener
    public ItemClickListener mListener;

    public EmergencyPhoneNumberAdapter(Context mContext, ArrayList<EmergencyPhoneCategory> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public EmergencyPhoneNumberAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_emergency, parent, false);

        return new EmergencyPhoneNumberAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EmergencyPhoneNumberAdapter.MyViewHolder holder, int position) {
        EmergencyPhoneCategory emergency = dataList.get(position);

        holder.tv_emergency_name.setText(emergency.getName());
        Glide
                .with(mContext)
                .load(emergency.getImgUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.iv_emergency_logo);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_emergency_logo)
        public ImageView iv_emergency_logo;
        @BindView(R.id.tv_emergency_name)
        public TextView tv_emergency_name;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemListener(itemView,getAdapterPosition());
                }
            });
        }
    }

    public void setItemClickListener(ItemClickListener mListener) {
        if (mListener != null) {
            this.mListener = mListener;
        }
    }
}
