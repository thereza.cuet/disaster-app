package com.android.disasterresponse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.disasterresponse.R;
import com.android.disasterresponse.listener.ItemClickListener;
import com.android.disasterresponse.model.DisasterType;
import com.android.disasterresponse.model.News;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DisasterTypeAdapter extends RecyclerView.Adapter<DisasterTypeAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<DisasterType> dataList;
    // Listener
    public ItemClickListener mListener;

    public DisasterTypeAdapter(Context mContext, ArrayList<DisasterType> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_disaster_type, parent, false);

        return new DisasterTypeAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        DisasterType disasterType = dataList.get(position);

        holder.tv_disaster_type_name.setText(disasterType.getName());

        Glide.with(mContext)
                .load(disasterType.getImgUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.iv_disaster_type);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_disaster_type)
        public ImageView iv_disaster_type;
        @BindView(R.id.tv_disaster_type_name)
        public TextView tv_disaster_type_name;

        public MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemListener(itemView,getAdapterPosition());
                }
            });

        }
    }
    public void setItemClickListener(ItemClickListener mListener) {
        if (mListener != null) {
            this.mListener = mListener;
        }
    }

}
