package com.android.disasterresponse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.disasterresponse.R;
import com.android.disasterresponse.listener.ItemClickListener;
import com.android.disasterresponse.model.EmergencyPhoneCategory;
import com.android.disasterresponse.model.EmergencyPhoneNumber;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmergencyPhoneNoDetailsAdapter extends RecyclerView.Adapter<EmergencyPhoneNoDetailsAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<EmergencyPhoneNumber> dataList;
    // Listener
    public ItemClickListener mListener;

    public EmergencyPhoneNoDetailsAdapter(Context mContext, ArrayList<EmergencyPhoneNumber> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public EmergencyPhoneNoDetailsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_emergency_phone, parent, false);

        return new EmergencyPhoneNoDetailsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EmergencyPhoneNoDetailsAdapter.MyViewHolder holder, int position) {

        EmergencyPhoneNumber emergencyPhoneNumber = dataList.get(position);

        holder.tv_title.setText(emergencyPhoneNumber.getDescription());
        holder.tv_phone.setText(emergencyPhoneNumber.getMobileNumber());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_title)
        public TextView tv_title;

        @BindView(R.id.tv_phone)
        public TextView tv_phone;
        @BindView(R.id.iv_call)
        public ImageView iv_call;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemListener(itemView, getAdapterPosition());
                }
            });
        }
    }

    public void setItemClickListener(ItemClickListener mListener) {
        if (mListener != null) {
            this.mListener = mListener;
        }
    }
}
